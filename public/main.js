var points = [
  [Math.random() * 300 + 150, Math.random() * 100 + 15],
  [Math.random() * 140, Math.random() * 470 + 100],
  [Math.random() * 198 + 400, Math.random() * 198 + 400]
]

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

function redrawBasePoints() {
  points.forEach(point => {
    ctx.fillStyle = 'rgb(200, 0, 0)';
    ctx.fillRect(point[0], point[1], 6, 6);
  })
}
redrawBasePoints()

ctx.fillStyle = 'rgb(0, 240, 10)';
var currentPoint = [Math.floor(Math.random() * 500), Math.floor(Math.random() * 500)]

var POINT_SIZE = 4
ctx.fillRect(currentPoint[0], currentPoint[1], POINT_SIZE, POINT_SIZE);

function dropPoint() {
  var r = Math.random();
  var ri = Math.floor(r * 3)
  var point = points[ri];
  var nextX = Math.round((currentPoint[0] + point[0]) / 2)
  var nextY = Math.round((currentPoint[1] + point[1]) / 2)
  ctx.fillRect(nextX, nextY, POINT_SIZE, POINT_SIZE);
  currentPoint = [nextX, nextY]
  redrawBasePoints()
  ctx.fillStyle = 'rgb(100, 120, 270)';
  ctx.fillRect(point[0], point[1], 6, 6);
  ctx.fillStyle = 'rgb(0, 240, 10)';
}

var btn = document.getElementById('dropPoint')
var cntInput = document.getElementById('pointsCount')
btn.onclick = () => {
  var count = Number.parseInt(cntInput.value)
  console.log(count);
  for (var i=0; i < count; i++) {
    console.log('drop point')
    dropPoint()
  }
}
